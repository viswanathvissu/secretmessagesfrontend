import './sendMessage.css'
import * as React from 'react';
import { useState } from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';
import Card from '@mui/material/Card';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import TextareaAutosize from '@mui/material/TextareaAutosize';
import SendMessageImage from './sendMessage.jpg';
import Toast from './Toast';

export default function SendMessage(props) {

    const [message,setMessage] = useState("");
    const [open,setOpen] = useState(false);
    var name = props ? props.name : ""
    var sessionId = props ? props.sessionId : 'something'

    async function sendMessageFunc(){
        var response = await fetch('http://localhost:5000/message',{
            mode: 'cors',
            credentials: 'include',
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({
                sessionId: sessionId,
                message: message
            })
        }).then((response)=>{
            if (response.status==200){
                setOpen(true);
            }
        })
    }

    function createLink(){
        window.location.href = "http://localhost:3000";
    }

  return (
    <Card sx={{ maxWidth: 345 }}>
        <CardMedia
            component="img"
            alt="green iguana"
            height="300"
            image= {SendMessageImage}
        />
        <CardContent>
            <h6 className="sendMsg">
            Enter your Secret message to {name}
            </h6>
            <Box
            sx={{
                width: 700,
                maxWidth: '100%',
            }}
        >
            <TextareaAutosize
                aria-label="minimum height"
                minRows={3}
                value={message}
                onChange={ e => setMessage(e.target.value) }
                className="MessageArea"
                placeholder="Enter your secret message"
            />
            <Button style={{
            fontFamily : 'Permanent Marker'
        }} variant="contained" onClick={sendMessageFunc} className="MessageArea">Send Message</Button>
            </Box>
        </CardContent>
        <Button style={{
            backgroundColor: "#21b6ae",
            padding: "18px 36px",
            fontSize: "18px",
            fontFamily : 'Permanent Marker'
        }} variant="contained" onClick={createLink} className="MessageArea">Create your own link</Button>
        <Toast isOpen={open} setOpen={setOpen}/>
    </Card>
  );
}