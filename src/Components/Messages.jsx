import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import ListSubheader from '@mui/material/ListSubheader';
import MessageCards from './MessageCards.jsx'

export default class Messages extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      records:[]
    };
  }

  async componentDidMount() {
    fetch('http://localhost:5000/messages/'+this.props.sessionId)
  .then((response) => {
    return response.json();
  }).then((data) => {
      this.setState({"records":data})
  })
  }

  render() {
    return (
      <List
        sx={{
          width: '100%',
          maxWidth: 500,
          height:   '100%',
          position: 'relative',
          overflow: 'auto',
          '& ul': { padding: 0 },
        }}
        subheader={<li />}
      >
        {
        this.state.records.length>0 ? this.state.records.map((sectionId,i) => (
          <li key={`section-${sectionId}`}>
            <ul>
              <ListSubheader><MessageCards number={i} message={sectionId.message} /></ListSubheader>
            </ul>
          </li>
        )):<h2>No Messages</h2>}
      </List>
    );
  }
}