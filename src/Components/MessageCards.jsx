import * as React from 'react';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import Typography from '@mui/material/Typography';
import { CardActionArea } from '@mui/material';
import './MessageCard.css';

export default class MessageCards extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      records:[]
    };
  }

  render() {
    return (
    <Card>
      <CardActionArea>
        <CardContent>
          <h6 className="sendMsg">
            {`Message ${this.props.number+1}`}
          </h6>
          <p className="sendMsgPara">
            {this.props.message}
          </p>
        </CardContent>
      </CardActionArea>
    </Card>
  );
  }
}
