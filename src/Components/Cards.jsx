import * as React from 'react';
import Card from '@mui/material/Card';
import {useState} from 'react';
import CardActions from '@mui/material/CardActions';
import CardContent from '@mui/material/CardContent';
import CardMedia from '@mui/material/CardMedia';
import TextField from '@mui/material/TextField';
import Button from '@mui/material/Button';
import Typography from '@mui/material/Typography';
import secret from './secret.jpg';
import './Cards.css';

export default function Cards() {

    const [name,setName] = useState("")

    async function getCookie() {
        var enteredName = name;
        let response = await fetch(`http://localhost:5000/signin`,{
            mode: 'cors',
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            credentials: 'include',
            body: JSON.stringify({
                names: enteredName,
            })
        });
        response = await response.json();
        var a = document.getElementById('link');
        a.innerHTML= window.location.href+"secret/"+(response.sessionId);
        a.href = window.location.href+"secret/"+(response.sessionId);
      };

  return (
    <Card sx={{ maxWidth: 345, height:600 }}>
      <CardMedia
        component="img"
        height="200"
        image= {secret}
        alt="secret"
      />
      <CardContent>
        <div className="cardContent">
            <h5 className="sendMsg">
                CREATE YOUR SECERET MESSAGE LINK
            </h5>
            <TextField style={{
        fontFamily : 'Permanent Marker'
    }} id="standard-basic" label="Enter your name" variant="standard" onChange={(e)=>{
                setName(e.target.value)
            }} />
            <Button type="primary" style={{
        fontFamily : 'Permanent Marker'
    }}onClick={getCookie}>Generate link</Button>
    <br/>
            <a id="link"></a>
        </div>
      </CardContent>
    </Card>
  );
}