import './App.css';
import React, { Component } from "react";
import ReactDOM from "react-dom";
import Cards from './Components/Cards.jsx';
import Messages from './Components/Messages.jsx';
import SendMessage from './Components/SendMessage';
import Cookies from 'js-cookie';

class App extends React.Component {
  constructor(props) {
    super(props);
    var cookie = Cookies.get('session_id')
    this.state = {
      isCookie: false,
      isQuestion: false,
      name: "",
      clientId: "",
      cookie: cookie ? cookie : "",
      isClient: false
    };
  }


  async componentDidMount() {
    var location = window.location.href;
    var hostName = window.location.hostname
    if (location.includes('/secret')){
      var k = location.indexOf('/secret')
      k = k+8
      var url = location.substring(k,location.length)
      if (this.state.cookie == url){
        var newObj = Object.assign({},this.state)
        newObj['isClient'] = true;
        this.setState(newObj)
      }
      var response = await fetch('http://localhost:5000/secret/'+url,{
        mode: 'cors',
        method: 'GET',
        headers: {'Content-Type': 'application/json'},
      }).then((response) =>{
        return response.json();
      }).then((data)=>{
        var newObj = Object.assign({},this.state)
        newObj['isQuestion'] = true;
        newObj['clientId'] = url;
        newObj['name']= data.result.name;
        this.setState(newObj)
      })
      
    }
    else{
      console.log("Heyyy i'm in useEffect ouside")
      var response = await fetch('http://localhost:5000/validate',{
      mode: 'cors',
      credentials: 'include'
      });
      if (response.status == 200){
        var newObj = Object.assign({},this.state)
        newObj['isCookie'] = true;
        this.setState(newObj)
      }
  }
  }
  render() {
    return (
      <div className="App">
        <div className="App-header">
          {this.state.isClient ? <Messages sessionId={this.state.cookie} /> : this.state.isQuestion ? <SendMessage sessionId={this.state.clientId} name={this.state.name} /> : this.state.isCookie ? <Messages sessionId={this.state.cookie} /> : <Cards />}
        </div>
      </div>
    );
  }
}
export default App;
